// Lab1 - HelloOldOpenGL.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

#include <GL/glew.h> 
#include <GL/freeglut.h>

#pragma comment (lib, "freeglut.lib")

void InitGLUT(int argc, char* argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowPosition(100, 100); // pozitia initiala a ferestrei
	glutInitWindowSize(1000, 700); //dimensiunile ferestrei
}

void Initialize(void)
{
	glClearColor(0.0f, 0.0f, 1.0f, 1.0f); // culoarea negru de fond a ecranului
}

void RenderFunction(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	glPointSize(20.0);
	glBegin(GL_POINTS);
	// primul varf
	glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
	glVertex4f(-0.8f, 0.8f, 0.0f, 1.0f);
	// al doilea varf
	glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
	glVertex4f(0.0f, 0.0f, 0.0f, 1.0f);
	// al treilea varf
	glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
	glVertex4f(0.8f, -0.8f, 0.0f, 1.0f);
	// al patrulea varf
	glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
	glVertex4f(0.8f, 0.8f, 0.0f, 1.0f);
	// al cincilea varf
	glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
	glVertex4f(-0.8f, -0.8f, 0.0f, 1.0f);
	glEnd();
	glFlush();
}

int main(int argc, char* argv[])
{
	InitGLUT(argc, argv);
	glutCreateWindow("Primul triunghi - OpenGL <<vechi>>"); // titlul ferestrei
	printf("OpenGL version supported by this platform: (%s) \n", glGetString(GL_VERSION));
	printf("GLSL version supported by this platform: (%s) \n", glGetString(GL_SHADING_LANGUAGE_VERSION));
	Initialize();
	glutDisplayFunc(RenderFunction);
	glutMainLoop();
	return 0;
}


